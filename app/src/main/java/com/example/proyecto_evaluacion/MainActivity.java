package com.example.proyecto_evaluacion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {
    EditText txtNombre, txtDireccion, txtCorreo, txtNumero, txtReferencia ;
    ImageButton btnAgregar;
    String nombree, direccion, correo, numero, referencia;
    private FloatingActionButton btnLlamar;
    private final int PHONE_CALL_CODE=100;
    private final int CAMERA_CALL_CODE=120;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtDireccion = (EditText) findViewById(R.id.txtDireccion);
        txtCorreo = (EditText) findViewById(R.id.txtCorreo);
        txtNumero = (EditText) findViewById(R.id.txtTelefono);
        txtReferencia = (EditText) findViewById(R.id.txtReferencia);
        btnAgregar = (ImageButton) findViewById(R.id.btnAgregar);
        btnLlamar = findViewById(R.id.btnLlamar);


        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nombree= txtNombre.getText().toString();
                direccion= txtDireccion.getText().toString();
                correo=txtCorreo.getText().toString();
                numero=txtNumero.getText().toString();
                referencia=txtReferencia.getText().toString();

                if (nombree.equals("")|| direccion.equals("")||correo.equals("")||numero.equals("")||referencia.equals("")){
                    Toast.makeText(MainActivity.this, "LLENA LOS DATOS CORRESPONDIENTES", Toast.LENGTH_SHORT).show();
                } else {
                    Intent a = new Intent(v.getContext(), ProductosActivity.class);
                    a.putExtra("Nombre",   nombree);
                    startActivity(a);
                }
            }
        });

        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num= "5617736115";
                if (num != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                    }else{
                        versionesAnteriores(num);
                    }
                }
            }

            private void versionesAnteriores(String num){
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel"+num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada);
                }else{
                    Toast.makeText(MainActivity.this, "Acepta los permisos", Toast.LENGTH_SHORT).show();
                }
            }
            private void versionNueva(){

            }

        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result= grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    if (result== PackageManager.PERMISSION_GRANTED){
                        String PhoneNumber= "5617736115";
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+PhoneNumber));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamada);
                    }else{
                        Toast.makeText(this, "No se acepto el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case CAMERA_CALL_CODE:
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }

    }

    private boolean verificarPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado== PackageManager.PERMISSION_GRANTED;
    }
}