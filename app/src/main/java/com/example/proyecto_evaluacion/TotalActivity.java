package com.example.proyecto_evaluacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TotalActivity extends AppCompatActivity {
    TextView cant1, cant2, cant3, cant4, cant5, total1, total2, total3, total4, total5, total;
    Button btnConfirmar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total);


        cant1= (TextView) findViewById(R.id.cantFresas);
        cant2= (TextView) findViewById(R.id.cantManzanas);
        cant3= (TextView) findViewById(R.id.cantPlatanos);
        cant4= (TextView) findViewById(R.id.cantBanderilla);
        cant5= (TextView) findViewById(R.id.cantDoritos);

        total1= (TextView) findViewById(R.id.totalFresas);
        total2= (TextView) findViewById(R.id.totalManzanas);
        total3= (TextView) findViewById(R.id.totalPlatano);
        total4= (TextView) findViewById(R.id.totalBanderilla);
        total5= (TextView) findViewById(R.id.totalDoritos);

        total= (TextView) findViewById(R.id.total2);


        btnConfirmar=(Button) findViewById(R.id.btnConfirmar);

        String cantFresas = getIntent().getExtras().getString("nFresas");
        String totalFresas = getIntent().getExtras().getString("totalFresas");
        cant1.setText(cantFresas);
        total1.setText("$"+totalFresas);

        String cantManzanas = getIntent().getExtras().getString("nManzanas");
        String totalManzanas = getIntent().getExtras().getString("totalManzanas");
        cant2.setText(cantManzanas);
        total2.setText("$"+totalManzanas);

        String cantPlatanos = getIntent().getExtras().getString("nPlatanos");
        String totalPlatanos = getIntent().getExtras().getString("totalPlatanos");
        cant3.setText(cantPlatanos);
        total3.setText("$"+totalPlatanos);

        String cantBandarillas = getIntent().getExtras().getString("nBanderillas");
        String totalBanderillas = getIntent().getExtras().getString("totalBanderillas");
        cant4.setText(cantBandarillas);
        total4.setText("$"+totalBanderillas);

        String cantDorilocos = getIntent().getExtras().getString("nDorilocos");
        String totalDorilocos = getIntent().getExtras().getString("totalDorilocos");
        cant5.setText(cantDorilocos);
        total5.setText("$"+totalDorilocos);

        String totalTotal = getIntent().getExtras().getString("total");
        total.setText("$"+totalTotal);

        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(v.getContext(), EnvioActivity.class);
                startActivity(a);
            }
        });
    }
}