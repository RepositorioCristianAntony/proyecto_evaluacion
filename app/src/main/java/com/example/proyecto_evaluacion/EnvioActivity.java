package com.example.proyecto_evaluacion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class EnvioActivity extends AppCompatActivity {
        ImageButton btnLlamar;
    private final int PHONE_CALL_CODE=100;
    private final int CAMERA_CALL_CODE=120;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_envio);
        btnLlamar= (ImageButton) findViewById(R.id.btnVerificar);

        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num= "5617736115";
                if (num != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                    }else{
                        versionesAnteriores(num);
                    }
                }
            }

            private void versionesAnteriores(String num){
                Intent intentLlamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel"+num));
                if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(intentLlamada);
                }else{
                    Toast.makeText(EnvioActivity.this, "Acepta los permisos", Toast.LENGTH_SHORT).show();
                }
            }
            private void versionNueva(){

            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result= grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    if (result== PackageManager.PERMISSION_GRANTED){
                        String PhoneNumber= "5617736115";
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+PhoneNumber));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamada);
                    }else{
                        Toast.makeText(this, "No se acepto el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case CAMERA_CALL_CODE:
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }

    }

    private boolean verificarPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado== PackageManager.PERMISSION_GRANTED;
    }
}