package com.example.proyecto_evaluacion;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class ProductosActivity extends AppCompatActivity {
    TextView txtBienvenida;
    Spinner spnFresas, spnManzanas, spnPlatanos, spnBanderillas, spnDorilocos;
    Button btnContinuar;
    String cantFresas, cantManzanas, cantPlatanos, cantBanderillas, cantDorilocos;
    int nFresas, nManzanas, nPlatanos, nBanderillas, nDorilocos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos);
        txtBienvenida =(TextView) findViewById(R.id.txtBienvenida);
        String nombre = getIntent().getExtras().getString("Nombre");
        txtBienvenida.setText("Bienvenido "+nombre);
        spnFresas=(Spinner) findViewById(R.id.spnFresas);
        spnManzanas=(Spinner) findViewById(R.id.spnManzanas);
        spnPlatanos=(Spinner) findViewById(R.id.spnPlatanos);
        spnBanderillas=(Spinner) findViewById(R.id.spnBanderillas);
        spnDorilocos=(Spinner) findViewById(R.id.spnDorilocos);
        btnContinuar=(Button) findViewById(R.id.btnContinuar);
        ArrayList<String> cantidadesList= new ArrayList<String>();
        for (int i=0;i<=5;i++){
            cantidadesList.add(i+"");
        }
        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, cantidadesList);
        spnFresas.setAdapter(adapter);
        spnFresas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cantFresas= parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnManzanas.setAdapter(adapter);
        spnManzanas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cantManzanas=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnPlatanos.setAdapter(adapter);
        spnPlatanos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cantPlatanos=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnBanderillas.setAdapter(adapter);
        spnBanderillas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cantBanderillas=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnDorilocos.setAdapter(adapter);
        spnDorilocos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cantDorilocos=parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            nFresas=Integer.parseInt(cantFresas);
                String totalFresas= (nFresas * 30)+"";
            nManzanas=Integer.parseInt(cantManzanas);
                String totalManzanas= (nManzanas*30)+"";
            nPlatanos=Integer.parseInt(cantPlatanos);
                String totalPlatanos= (nPlatanos*25)+"";
            nBanderillas=Integer.parseInt(cantBanderillas);
                String totalBanderillas=(nBanderillas*25)+"";
            nDorilocos=Integer.parseInt(cantDorilocos);
                String totalDorilocos= (nDorilocos * 35)+"";

            String Total = ((nFresas * 30)+(nManzanas*30)+(nPlatanos*25)+(nBanderillas*25)+(nDorilocos * 35))+"";


                Intent a = new Intent(v.getContext(), TotalActivity.class);
                a.putExtra("nFresas",cantFresas);
                a.putExtra("totalFresas", totalFresas);
                a.putExtra("nManzanas", cantManzanas);
                a.putExtra("totalManzanas", totalManzanas);
                a.putExtra("nPlatanos", cantPlatanos);
                a.putExtra("totalPlatanos", totalPlatanos);
                a.putExtra("nBanderillas", cantBanderillas);
                a.putExtra("totalBanderillas", totalBanderillas);
                a.putExtra("nDorilocos", cantDorilocos);
                a.putExtra("totalDorilocos", totalDorilocos);
                a.putExtra("total", Total);
                startActivity(a);
            }
        });


    }
}